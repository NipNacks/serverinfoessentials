package me.nipnacks.serverinfoessentials.events;

import me.nipnacks.serverinfoessentials.ServerInfoEssentials;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerFirstJoin implements Listener {

    public ServerInfoEssentials plugin;

    public PlayerFirstJoin(ServerInfoEssentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        if (!e.getPlayer().hasPlayedBefore()){
            Player player = (Player) e.getPlayer();
            Location loc = (Location) player.getLocation();
            String welcome = plugin.getConfig().getString("welcomemessage");

            player.getServer().broadcastMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + welcome + ChatColor.YELLOW + player.getDisplayName());
            player.getLocation().getWorld().playSound(loc, Sound.BLOCK_NOTE_BLOCK_CHIME, 1, 1);
        }
    }

}

package me.nipnacks.serverinfoessentials.events;

import me.nipnacks.serverinfoessentials.ServerInfoEssentials;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinAgain implements Listener {

    public ServerInfoEssentials plugin;

    public PlayerJoinAgain(ServerInfoEssentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e){
        Player player = e.getPlayer();
        Location loc = (Location) player.getLocation();
        String playerjoinagainmessage = plugin.getConfig().getString("playerjoinagainmessage");

        player.getLocation().getWorld().playSound(loc, Sound.BLOCK_NOTE_BLOCK_HARP, 1, 1);
        e.setJoinMessage(ChatColor.LIGHT_PURPLE + "[" + ChatColor.GOLD + "+" + ChatColor.LIGHT_PURPLE + "]" + ChatColor.YELLOW + player.getDisplayName());
        player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + playerjoinagainmessage + ChatColor.YELLOW + player.getDisplayName());
    }
}

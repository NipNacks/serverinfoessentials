package me.nipnacks.serverinfoessentials.events;

import me.nipnacks.serverinfoessentials.ServerInfoEssentials;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeaveAgain implements Listener {

    public ServerInfoEssentials plugin;

    public PlayerLeaveAgain(ServerInfoEssentials plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerLeaveAgain(PlayerQuitEvent e){
        Player player = e.getPlayer();
        Location loc = (Location) player.getLocation();
        String playerleaveagainmessage = plugin.getConfig().getString("playerleaveagainmessage");

        player.getLocation().getWorld().playSound(loc, Sound.BLOCK_NOTE_BLOCK_HARP, 1, 1);
        e.setQuitMessage(ChatColor.LIGHT_PURPLE + "[" + ChatColor.GOLD + "-" + ChatColor.LIGHT_PURPLE + "]" + ChatColor.YELLOW + player.getDisplayName());
        player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + playerleaveagainmessage + ChatColor.YELLOW + player.getDisplayName());
    }
}

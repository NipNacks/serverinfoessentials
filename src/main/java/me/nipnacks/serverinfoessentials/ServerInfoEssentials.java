package me.nipnacks.serverinfoessentials;

import me.nipnacks.serverinfoessentials.commands.DiscordCommand;
import me.nipnacks.serverinfoessentials.commands.StoreCommand;
import me.nipnacks.serverinfoessentials.events.PlayerFirstJoin;
import me.nipnacks.serverinfoessentials.events.PlayerJoinAgain;
import me.nipnacks.serverinfoessentials.events.PlayerLeaveAgain;
import org.bukkit.plugin.java.JavaPlugin;

public final class ServerInfoEssentials extends JavaPlugin{

    @Override
    public void onEnable() {
        getLogger().info("ServerInfoEssentials is now starting up!");

        getConfig().options().copyDefaults();
        saveDefaultConfig();

        getCommand("store").setExecutor(new StoreCommand(this));
        getCommand("discord").setExecutor(new DiscordCommand(this));
        getServer().getPluginManager().registerEvents(new PlayerLeaveAgain(this), this);
        getServer().getPluginManager().registerEvents(new PlayerJoinAgain(this),this);
        getServer().getPluginManager().registerEvents(new PlayerFirstJoin(this),this);
    }

    @Override
    public void onDisable() {
        getLogger().info("ServerInfoEssentials is now Shutting down!");
    }
}

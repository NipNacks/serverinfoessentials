package me.nipnacks.serverinfoessentials.commands;

import me.nipnacks.serverinfoessentials.ServerInfoEssentials;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DiscordCommand implements CommandExecutor {

    public ServerInfoEssentials plugin;

    public DiscordCommand(ServerInfoEssentials plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player){
            Player player = (Player) sender;
            Location loc = (Location) player.getLocation();
            String discord = plugin.getConfig().getString("discordmessage");

            player.sendMessage(ChatColor.LIGHT_PURPLE + "" + ChatColor.ITALIC + "Here is our Discord Link!");
            player.sendMessage(ChatColor.YELLOW + "" + ChatColor.UNDERLINE + discord);
            player.getLocation().getWorld().playSound(loc, Sound.BLOCK_NOTE_BLOCK_CHIME, 1,1);
        }

        return false;
    }
}
